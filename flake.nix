{
  inputs = {
    devshell.url = github:numtide/devshell;
    flake-compat = {
      url = github:edolstra/flake-compat;
      flake = false;
    };
    flake-utils.url = github:numtide/flake-utils;
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";
  };

  outputs = inputs:
    with inputs;
      {
        overlays.default = import ./overlay.nix;
      }
      // flake-utils.lib.eachDefaultSystem (system: let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            devshell.overlay
            self.outputs.overlays.default
          ];
        };
        precommix = import ./precommix.nix;
      in {
        devShells.default = pkgs.devshell.mkShell {
          imports = [precommix.devshellModules.${system}.default];
        };
        packages = {
          default = pkgs.python3Packages.ansible-core;
        };
        lib = {
          inherit
            (pkgs)
            fetchAnsibleGalaxyCollection
            mkAnsibleCollections
            mkAnsibleGalaxyCollectionSrc
            ;
        };
        checks = pkgs.callPackages ./tests {};
      });
}
