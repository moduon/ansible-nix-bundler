{
  ansible-core ? python3Packages.ansible-core,
  callPackage,
  makeWrapper,
  python3Packages,
  stdenv,
}: roles: let
  roleAtt = attName: builtins.map (role: role.${attName}) roles;
  drv = stdenv.mkDerivation rec {
    inherit (ansible-core) version pname;
    nativeBuildInputs = [makeWrapper ansible-core];

    ANSIBLE_HOME = "/build/.ansible";
    roleCount = builtins.length roles;

    # `roles` must be a list with all these attributes
    roleNames = roleAtt "name";
    srcs = roleAtt "src";
    roleVersions = roleAtt "version";

    sourceRoot = "/build/sources";
    unpackCmd = ''
      # Unpack each source in a predictable directory
      dest="$sourceRoot/$(basename $curSrc)"
      mkdir -p "$dest"
      cd "$dest"

      # HACK Make this hook fail to continue with the default ones
      false
    '';

    # DOCS https://galaxy.ansible.com/docs/using/installing.html#installing-multiple-roles-from-a-file
    requirementsJSON = builtins.toJSON (
      builtins.map (role: {
        inherit (role) name version;
        src = "tarballs/${role.name}.tar.gz";
      })
      roles
    );
    buildPhase = ''
      # Create the tarballs that ansible-galaxy needs
      cd $TMPDIR
      mkdir -p tarballs
      for num in $(seq 1 $roleCount); do
        rName=$(echo $roleNames | cut -d ' ' -f $num)
        rSrc=$(echo $srcs | cut -d ' ' -f $num)
        rUnpacked=$sourceRoot/$(basename $rSrc)
        rRepacked="tarballs/$rName.tar.gz"

        tar -czf $rRepacked -C $rUnpacked .
      done

      # Create the requirements file
      echo "$requirementsJSON" > requirements.yaml
    '';

    installPhase = ''
      mkdir -p $out/roles
      ansible-galaxy role install -p $out/roles -r requirements.yaml
    '';

    # Wrap Ansible binaries to find these roles
    postFixup = ''
      mkdir -p $out/bin
      for executable in ${ansible-core}/bin/*; do
        makeWrapper "$executable" "$out/bin/$(basename $executable)" \
          --prefix ANSIBLE_ROLES_PATH : "$out/roles"
      done
    '';

    # Make sure Ansible finds the roles
    doInstallCheck = true;
    installCheckPhase = ''
      for num in $(seq 1 $roleCount); do
        rName=$(echo $roleNames | cut -d ' ' -f $num)
        rVersion=$(echo $roleVersions | cut -d ' ' -f $num)

        echo Matching role versions found
        $out/bin/ansible-galaxy role list $cName

        echo Asserting installation of $rName $rVersion
        $out/bin/ansible-galaxy role list $rName | grep "$rName, $rVersion"
      done
    '';
  };
in
  drv
  // {
    withCollections = callPackage ./withCollections.nix {ansible-core = drv;};
    withRoles = callPackage ./withRoles.nix {ansible-core = drv;};
  }
