{
  fetchurl,
  lib,
}: {
  name,
  sha256 ? "",
  version,
} @ inputs: let
  collection = lib.last splitName;
  namespace = builtins.head splitName;
  splitName = lib.splitString "." name;
in
  assert builtins.length splitName == 2;
    fetchurl {
      inherit sha256;
      url = "https://galaxy.ansible.com/download/${namespace}-${collection}-${version}.tar.gz";
    }
    // {passtrhu = inputs;}
