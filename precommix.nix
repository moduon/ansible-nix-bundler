# Get precommix from separate location, synced with last template update
import (builtins.fetchGit {
  url = "https://gitlab.com/moduon/precommix.git";
  ref = "main";
  rev = "b55e4fa49345c6b1a20836e5ed9cbba3cb3a1394";
})
