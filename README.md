# Ansible Nix Bundler

Helpful functions to package
[Ansible roles](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_roles.html)
and [collections](https://docs.ansible.com/ansible/latest/collections_guide/index.html)
with [Nix](https://nixos.org/) and make them available to
[Ansible](https://www.ansible.com/).

# Why

Because, as every software in the world, Ansible roles and collections should be easy to
package with Nix.

# How

Add the provided overlay when loading Nixpkgs.

Then use the provided packages:

-   `python3Packages.ansible-core.withCollections` to add some collection(s) to Ansible.
-   `python3Packages.ansible-core.withRoles` to add some role(s) to Ansible.
-   `fetchAnsibleGalaxyCollection` to download a collection from Ansible Galaxy.
-   `mkAnsibleCollections` to create a pseudo-python package that includes the
    collections. This way, you can use it in `python3.withPackages` to create an
    environment.
-   `mkAnsibleGalaxyCollectionSrc` to download a collection from Ansible Galaxy and
    produce one item properly formatted to be passed to `withCollections`.

See the examples:

-   [Adding roles to Ansible](./tests/roleWithDeps.nix)
-   [Adding collections to Ansible](./tests/collectionPrebuiltWithDeps.nix)
-   [Creating a Python environment that includes Ansible, one collection and another Python dependency](./tests/collectionsEnv.nix)
