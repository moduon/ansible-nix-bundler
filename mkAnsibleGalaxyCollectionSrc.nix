{fetchAnsibleGalaxyCollection}: {
  name,
  sha256 ? "",
  version,
} @ inputs: {
  inherit name version;
  src = fetchAnsibleGalaxyCollection inputs;
}
