{
  ansible-core ? python3Packages.ansible-core,
  python3Packages,
}:
ansible-core.withRoles [
  # DOCS https://galaxy.ansible.com/geerlingguy/php
  rec {
    name = "geerlingguy.php";
    version = "4.8.0";
    src = builtins.fetchurl {
      url = "https://github.com/geerlingguy/ansible-role-php/archive/refs/tags/${version}.tar.gz";
      sha256 = "sha256-YOP91yM08Neqzw6vmtpGwlotfOV8NXPfb1quN3VJWLo=";
    };
  }
]
