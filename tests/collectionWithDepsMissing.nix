{
  ansible-core ? python3Packages.ansible-core,
  python3Packages,
  testers,
}:
# Install geerlingguy.mac collection, which depends on community.general and
# will fail
testers.testBuildFailure (
  ansible-core.withCollections [
    # DOCS https://galaxy.ansible.com/geerlingguy/mac
    rec {
      name = "geerlingguy.mac";
      version = "2.1.1";
      # Prebuilt in Ansible Galaxy, no need to patch like in ./collectionWithDeps.nix
      src = builtins.fetchurl {
        url = "https://galaxy.ansible.com/download/geerlingguy-mac-${version}.tar.gz";
        sha256 = "sha256:0i690dk9g1lxad4hmd4s32wlrpj5s10qcz5bbw7r912jgbs5f543";
      };
    }
  ]
)
