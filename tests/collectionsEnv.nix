{
  mkAnsibleCollections,
  mkAnsibleGalaxyCollectionSrc,
  python3,
  runCommand,
}:
runCommand "python-env-with-collections" rec {
  ANSIBLE_HOME = "/build/.ansible";
  collections = mkAnsibleCollections [
    (mkAnsibleGalaxyCollectionSrc {
      name = "geerlingguy.k8s";
      version = "0.10.0";
      sha256 = "sha256-sZEs4mhqCcvP1UdPxYkh5I6Nnc9YP/9ToKA8979xw9g=";
    })
  ];
  env = python3.withPackages (ps: [
    collections
    ps.ansible-core
    ps.kubernetes
  ]);
  goodPlaybook = ''
    - hosts: localhost
      roles:
        - role: geerlingguy.k8s.helm
  '';
} ''
  # Test playbook works
  echo "$goodPlaybook" > good.yaml
  $env/bin/ansible-playbook -vvvvv --list-tasks good.yaml

  # Test the provided python environment has the extra dependency
  $env/bin/python -c 'import kubernetes'

  touch $out
''
