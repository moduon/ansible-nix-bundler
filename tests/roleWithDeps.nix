{
  ansible-core ? python3Packages.ansible-core,
  fetchFromGitHub,
  python3Packages,
  testers,
}:
# This role has dependencies on other roles; all are included
ansible-core.withRoles [
  # Main role
  # DOCS https://galaxy.ansible.com/andrewrothstein/miniconda
  rec {
    name = "andrewrothstein.miniconda";
    version = "6.1.7";
    src = builtins.fetchurl {
      url = "https://github.com/andrewrothstein/ansible-miniconda/archive/refs/tags/v${version}.tar.gz";
      sha256 = "sha256:1bs81ghf6l3sakxm45w5qj921dn1nkqpcnl4slq029794pq2hfqr";
    };
  }

  # Dependencies
  rec {
    name = "andrewrothstein.bash";
    version = "1.1.11";
    # Notice that this dependency is downloaded and extracted...
    src = fetchFromGitHub {
      owner = "andrewrothstein";
      repo = "ansible-bash";
      rev = "v${version}";
      sha256 = "sha256-8nvpYpVGj/ZqNuBnmmu/8QzQsdutxCnuxYWVmHb0lfs=";
    };
  }

  rec {
    name = "andrewrothstein.unarchive-deps";
    version = "1.2.4";
    # ... but this one is downloaded directly as a tarball
    src = builtins.fetchurl {
      url = "https://github.com/andrewrothstein/ansible-unarchive-deps/archive/refs/tags/v${version}.tar.gz";
      sha256 = "sha256:0q97klpgxpk2dablhy80ks98f1nwzsxp212lsjxfby8y0xd5mlsj";
    };
  }
]
