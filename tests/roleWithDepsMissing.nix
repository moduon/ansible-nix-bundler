{
  ansible-core ? python3Packages.ansible-core,
  fetchFromGitHub,
  python3Packages,
  testers,
}:
# This role has dependencies and fails without them
testers.testBuildFailure (ansible-core.withRoles [
  # DOCS https://galaxy.ansible.com/andrewrothstein/miniconda
  rec {
    name = "andrewrothstein.miniconda";
    version = "6.1.7";
    src = builtins.fetchurl {
      url = "https://github.com/andrewrothstein/ansible-miniconda/archive/refs/tags/v${version}.tar.gz";
      sha256 = "sha256:1bs81ghf6l3sakxm45w5qj921dn1nkqpcnl4slq029794pq2hfqr";
    };
  }
])
