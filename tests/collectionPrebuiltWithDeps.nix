# Same as ./collectionWithDeps, but using prebuilt tarballs from Ansible Galaxy,
# which are pre-patched upstream on release and don't need the patchPhase.
{
  ansible-core ? python3Packages.ansible-core,
  mkAnsibleGalaxyCollectionSrc,
  python3Packages,
}:
ansible-core.withCollections [
  # DOCS https://galaxy.ansible.com/community/general
  # This is a dependency of geerlingguy.mac as seen in
  # https://github.com/geerlingguy/ansible-collection-mac/blob/2.1.1/galaxy.yml#L40
  (mkAnsibleGalaxyCollectionSrc {
    name = "community.general";
    version = "6.2.0";
    sha256 = "sha256-xcB+kKtR8p/L5qaCFbGDJ0h7CzSiBjdaMA0xR6jqPEk=";
  })

  # DOCS https://galaxy.ansible.com/geerlingguy/mac
  rec {
    name = "geerlingguy.mac";
    version = "2.1.1";
    src = builtins.fetchurl {
      url = "https://galaxy.ansible.com/download/geerlingguy-mac-${version}.tar.gz";
      sha256 = "sha256:0i690dk9g1lxad4hmd4s32wlrpj5s10qcz5bbw7r912jgbs5f543";
    };
  }
]
