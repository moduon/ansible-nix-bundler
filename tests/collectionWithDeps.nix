{
  ansible-core ? python3Packages.ansible-core,
  fetchFromGitHub,
  python3Packages,
}: let
  # Install geerlingguy.mac collection, which depends on community.general and
  # needs to be patched to install the appropriate version
  macVersion = "2.1.1";
  macSrc = fetchFromGitHub {
    owner = "geerlingguy";
    repo = "ansible-collection-mac";
    rev = macVersion;
    sha256 = "sha256-da4wX+MDbu4GPmiaAF8NRphSmhux0t/ZiLaBLFTEvc8=";
  };
  _ansible = ansible-core.withCollections [
    # DOCS https://galaxy.ansible.com/community/general
    # This is a dependency of geerlingguy.mac as seen in
    # https://github.com/geerlingguy/ansible-collection-mac/blob/2.1.1/galaxy.yml#L40
    rec {
      name = "community.general";
      version = "6.2.0";
      src = builtins.fetchurl {
        url = "https://github.com/ansible-collections/community.general/archive/refs/tags/${version}.tar.gz";
        sha256 = "sha256:0aq67pxzd223ikgycsmgjypcjrlwbzgpqwazzpw4d1fq445gqpcw";
      };
    }

    # DOCS https://galaxy.ansible.com/geerlingguy/mac
    {
      name = "geerlingguy.mac";
      version = macVersion;
      src = macSrc;
    }
  ];
in
  _ansible
  .overrideAttrs (old: {
    # Collection geerlingguy.mac auto-tags the version in
    # https://github.com/geerlingguy/ansible-collection-mac/blob/2.1.1/galaxy-deploy.yml#L41-L45
    # so we need to patch it or we'll be installing 1.2.0 as seen in
    # https://github.com/geerlingguy/ansible-collection-mac/blob/2.1.1/galaxy.yml#L4
    patchPhase = ''
      ${old.patchPhase or ""}
      echo Patching geerlingguy.mac version
      sed -i 's/version:.*/version: "${macVersion}"/' \
        $sourceRoot/$(basename ${macSrc})/*/galaxy.yml
    '';
  })
