{
  ansible-core ? python3Packages.ansible-core,
  mkAnsibleGalaxyCollectionSrc,
  python3Packages,
  fetchFromGitHub,
  runCommand,
}:
runCommand "ansible-collections-and-roles" {
  ANSIBLE_HOME = "/build/.ansible";
  ansible =
    ((ansible-core.withRoles [
        # DOCS https://galaxy.ansible.com/tecnativa/hetzner_rescue_installimage
        rec {
          name = "tecnativa.hetzner_rescue_installimage";
          version = "3.0.0";
          src = fetchFromGitHub {
            owner = "Tecnativa";
            repo = "ansible-role-hetzner-rescue-installimage";
            rev = "3.0.0";
            sha256 = "sha256-q5gwZgdNKeqsyOuLuFzF9LrNQfgu8Xdplv3ztBa4TWQ=";
          };
        }
      ])
      .withCollections [
        # DOCS https://galaxy.ansible.com/geerlingguy/php_roles
        (mkAnsibleGalaxyCollectionSrc {
          name = "geerlingguy.k8s";
          version = "0.10.0";
          sha256 = "sha256-sZEs4mhqCcvP1UdPxYkh5I6Nnc9YP/9ToKA8979xw9g=";
        })
      ])
    # Chaining roles yet again, to make sure all of these are properly inter-chainable
    .withRoles [
      # DOCS https://galaxy.ansible.com/geerlingguy/php
      rec {
        name = "geerlingguy.php";
        version = "4.8.0";
        src = fetchFromGitHub {
          owner = "geerlingguy";
          repo = "ansible-role-php";
          rev = version;
          sha256 = "sha256-T2gv+NO+OF2puS3MhMsVXJq5xpumtlKeI9Jgb9qc7FU=";
        };
      }
    ];
  goodPlaybook = ''
    - hosts: localhost
      roles:
        - role: tecnativa.hetzner_rescue_installimage
        - role: geerlingguy.k8s.helm
        - role: geerlingguy.php
  '';
  badPlaybook = ''
    - hosts: localhost
      roles:
        - role: not.existing.role
  '';
} ''
  # Test you can play the good one
  echo "$goodPlaybook" > good.yaml
  $ansible/bin/ansible-playbook -vvvvv --list-tasks good.yaml

  # Test you cannot play the bad one
  echo "$badPlaybook" > bad.yaml
  $ansible/bin/ansible-playbook -vvvvv --list-tasks bad.yaml || touch $out
''
