final: prev: {
  fetchAnsibleGalaxyCollection = final.callPackage ./fetchAnsibleGalaxyCollection.nix {};
  mkAnsibleCollections = final.callPackage ./mkAnsibleCollections.nix {};
  mkAnsibleGalaxyCollectionSrc = final.callPackage ./mkAnsibleGalaxyCollectionSrc.nix {};
  python3Packages =
    prev.python3Packages
    // {
      ansible-core =
        prev.python3Packages.ansible-core
        // {
          withCollections = final.callPackage ./withCollections.nix {};
          withRoles = final.callPackage ./withRoles.nix {};
        };
    };
}
