{
  ansible-core ? python3Packages.ansible-core,
  callPackage,
  makeWrapper,
  python3Packages,
  stdenv,
}: collections: let
  collectionAtt = attName: builtins.map (collection: collection.${attName}) collections;

  # TODO Reuse mkAnsiblecollections
  drv = stdenv.mkDerivation rec {
    inherit (ansible-core) version pname;
    nativeBuildInputs = [makeWrapper ansible-core];

    ANSIBLE_HOME = "/build/.ansible";
    collectionCount = builtins.length collections;

    # `collections` must be a list with all these attributes
    collectionNames = collectionAtt "name";
    srcs = collectionAtt "src";
    collectionVersions = collectionAtt "version";

    sourceRoot = "/build/sources";
    unpackCmd = ''
      # Unpack each source in a predictable directory
      dest="$sourceRoot/$(basename $curSrc)"
      mkdir -p "$dest"
      cd "$dest"

      # HACK Make this hook fail to continue with the default ones
      false
    '';

    buildPhase = ''
      runHook preBuild

      mkdir -p $TMPDIR/tarballs

      # Create the requirements file
      echo 'collections:' > requirements.yaml

      for num in $(seq 1 $collectionCount); do
        cName=$(echo $collectionNames | cut -d ' ' -f $num)
        cSrc=$(echo $srcs | cut -d ' ' -f $num)
        cVersion=$(echo $collectionVersions | cut -d ' ' -f $num)
        cBuilt=$TMPDIR/tarballs/$(echo $cName | tr '.' '-')-$cVersion.tar.gz
        cType=file
        cUnpacked=$sourceRoot/$(basename $cSrc)

        # If source has these files, it's already built and needs just to be tarred
        if [ -f $cUnpacked/FILES.json -a -f $cUnpacked/MANIFEST.json ]; then
          tar --xform 's:^\./::' -czf $cBuilt -C $cUnpacked .

        # If it has this file, then it needs to be built
        elif [ -f $cUnpacked/galaxy.yml ]; then
          ansible-galaxy collection build --output-path $TMPDIR/tarballs $cUnpacked

        # Many tarballs (e.g. from GitHub) have one extra directory within
        else
          ansible-galaxy collection build --output-path $TMPDIR/tarballs $cUnpacked/*
        fi

        # Fill requirements file
        echo "- {name: $cName, version: '$cVersion',
                 type: $cType, source: $cBuilt}" >> requirements.yaml
      done

      runHook postBuild
    '';

    # DOCS https://docs.ansible.com/ansible/devel/collections_guide/collections_installing.html#install-multiple-collections-with-a-requirements-file
    installPhase = ''
      runHook preInstall

      ansible-galaxy collection install --offline -r requirements.yaml -p $out/collections

      runHook postInstall
    '';

    # Wrap Ansible binaries to find these collections
    postFixup = ''
      mkdir -p $out/bin
      for executable in ${ansible-core}/bin/*; do
        makeWrapper "$executable" "$out/bin/$(basename $executable)" \
          --prefix ANSIBLE_COLLECTIONS_PATHS : "$out/collections"
      done
    '';

    # Make sure Ansible finds the collections
    doInstallCheck = true;
    installCheckPhase = ''
      for num in $(seq 1 $collectionCount); do
        cName=$(echo $collectionNames | cut -d ' ' -f $num)
        cVersion=$(echo $collectionVersions | cut -d ' ' -f $num)

        echo Matching collection versions found
        $out/bin/ansible-galaxy collection list $cName

        echo Asserting installation of $cName $cVersion
        $out/bin/ansible-galaxy collection list $cName | grep "$cName $cVersion"
      done
    '';
  };
in
  drv
  // {
    withCollections = callPackage ./withCollections.nix {ansible-core = drv;};
    withRoles = callPackage ./withRoles.nix {ansible-core = drv;};
  }
